# Discord Bot
This discord bot is a fork of catBot, and stripped down to just supply rook functions. Most of this readme is otherwise the same.

## Installing
This project requires the Discord.py library for Python 3 to function. Instructions can be found in their documentation, after which cloning this repo will give you a runnable version of the code. Creating a file named "TOKEN" with your bots Discord token is required for it to work.

This section is rather empty and I apologize for that. I am planning to make installation more detailed and easy in the future once the baseline features have been implimented.

## Configuration
Core configuration options are stored at the very top of the main.py file, however modules that give catBot its features come with their own potential configs. Each of these can create a file that is mostly altered by having the owner of the server run commands to the bot through Discord. Manual editing of these files may result in issues if you are unsure how they are formatted. This is all considered a pain point, and will hopefully be addressed in later iterations.