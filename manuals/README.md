# Manuals
Manuals are documentation both for those working on the bot, and mainly for users. They are a system that allows the creator of a module to write a quick help menu entry that can be found among help entries for all other modules in the normal catBot help menu. This is handled internally and not by another module, so this cannot be removed.

## Formatting
Manuals are formatted based on newlines and special characters at the start of a line. The first line to have a \# is considered to be the start of the manual, and the label for a specific manual page. Each \# holds all text that follows within a single embed, until the next \#, or the end of the file. Embeds will be named with the texts following the \# on the same line. The line immedietly following the \# will be the brief description of the intention of the module under the title. After those two are put in, all lines after follow this set of formatting rules:

### \n
Lines with nothing are a breakpoint, and will split into the next detailed section/command, with the next line that holds contents being the title of the next section.

### " "
Lines starting with non-format specific characters will not be formatted at all andd just be put in as is.

### >
Lines starting with ">" will replace the ">" with the bot's current prefix and the modules current name. This means making a line that starts with "> subcommand" will become "[PREFIX]module subcommand" when shown to users.

### ?
Lines starting with "?" will replace the "?" with all commands tied to that function within by the module's _command_override(). This allows shortcutted commands to be listed and defined dynamically as those shortcuts change.

### *
Lines starting with "\*" before all other formatting will not be counted for the start of the line, allowing characters like ">" and "?" to still serve their purpose. Instead, using a "\*" will entirely prevent users without administrative permissions to see the entry. These enteries will show up to them if an admin runs the help command where general users can see it, but user prompted help menus will omit these enteries.