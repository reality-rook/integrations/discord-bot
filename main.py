#
#
# CAT CONFIG! THIS IS WHERE YOU CONF YOUR CAT
#
#

# What users put in front of their commands when using the bot.
command_prefix = "Rook! "
# Where to find the login token. This file MUST be an entirely empty file containing nothing but the token.
token_path = "./config/TOKEN"

# A directory for modules to store data intended to be modifiable to those running the bot.
config_path = "./config"
# A directory for modules to store data between runtimes, but at not meant to be as modifiable for bot runners.
data_path = "./data"
# Where to find all extensions and modules.
module_path = "./modules"
# Where to find all module manuals for the help menu.
manual_path = "./manuals"

#
#
# CODE BELOW, DO NOT CHANGE ANYTHING BELOW UNLESS YOU INTEND TO REPROGRAM THE CORE FUNCTIONALITY OF CATBOT.
#
#

# Core libraries for Discord management
import asyncio
import discord
# Core libraries for file management
import os
import sys

# Find and load all valid modules (valid meaning a python file with a _start_up function that does not have a . at the front.)
sys.path.insert(1, module_path)
bot_modules = []
for potential_module in os.listdir(module_path):
    if not potential_module.startswith(".") and potential_module.endswith(".py"):
        potential_module = potential_module[:-3]
        exec("import " + potential_module)
        exec(potential_module + "._start_up('" + potential_module + "', '" + config_path + "', '" + data_path + "')")
        bot_modules.append(potential_module)



# Big Bad Code (Spooky)
class CatClient(discord.Client):
    # Currently no real need to use this function, just debugging.
    async def on_ready(self):
        print("Client started.")


    async def on_message(self, message):
        # Command Processing
        if not message.content.startswith(command_prefix):

            # Check for overrides before giving up.
            for extension in bot_modules:
                override_dict = eval(extension + "._command_override()")
                for override in override_dict.keys():
                    if message.content == override:
                        coroutine_to_execute = eval(extension + "." + override_dict[message.content] + "([], message)")
                        result = await coroutine_to_execute
                        if result != "":
                            await message.channel.send(result)
                        return
            return

        #
        # commands are broken down into this format: [PREFIX][COMMAND] [SUBCOMMAND] [COMMAND ARG1] [COMMAND ARG2]
        #
        # PREFIX was made above and means its harder to accidentally send commands.
        #
        # COMMAND is a general family of functions. For our purposes,
        #         this is another python file imported into this one.
        #
        # SUBCOMMANDD is the specific function to run in the family of functions,
        #             or in this case a function from the imported file.
        #             normally if this is not put in the message the "default" function
        #             is called instead which should be put into the command file.
        #
        # COMMANDARGS a list of every word after the two front ones. They are assumed
        #             to be params sent to the SUBCOMMAND functions.
        #

        command = ""
        sub_command = "_default"
        command_args = []
        full_command = message.content.split(command_prefix)[1]

        # try to avoid implimenting bypasses to this step as much as possible.
        character_whitelist = set("abcdefghijklmnopqrstuvwxyz1234567890 ")
        full_command = "".join(filter(character_whitelist.__contains__, full_command.lower()))
        message_spaces = full_command.count(" ")

        if message_spaces >= 0:
            command = full_command.split(" ")[0]
            if not (command in bot_modules) and command != "help":
                await message.channel.send("Command not found!")
                return

            if message_spaces > 0:
                sub_command = full_command.split(" ")[1]

                if message_spaces > 1:
                    command_args = full_command.split(" ")[2:]

            if not (sub_command in eval("dir(" + command + ")")) and command != "help":
                await message.channel.send("Sub Command not found!")
                return

        # Handle manual commands
        if command == "help":
            help_embed = discord.Embed()
            if message.author.id == message.guild.owner_id:
                help_embed.color = discord.Colour.from_str("#342a36")
            else:
                help_embed.color = discord.Colour.from_str("#e89df2")
            if sub_command == "_default" or not (sub_command in bot_modules):
                help_embed.title = "Manual"
                help_embed.description = "There are help menus for all of the following modules:"
                for module in bot_modules:
                    help_embed.description = help_embed.description + "\n- " + module
                await message.channel.send(embed=help_embed)
                return

            manual_string = ""
            try:
                with open(manual_path + "/" + sub_command + ".manual", "r") as manual_file:
                    manual_string = manual_file.read() + "\n#"
            except FileNotFoundError:
                await message.channel.send("Apologies, it would appear this module doesn't have a valid manual configured for it.")
                return

            description_queued = False
            field_queued = False
            to_field = {
                    "name":"",
                    "value":""
                }
            for line in manual_string.split("\n"):
                if description_queued:
                    help_embed.description = line
                    description_queued = False
                    field_queued = True
                    continue

                if line.startswith("*"):
                    if message.author.id != message.guild.owner_id:
                        continue
                    else:
                        line = line.split("*")[1]

                if line.startswith("#"):
                    if help_embed.title != None:
                        if to_field["value"] != "":
                            help_embed.add_field(name=to_field["name"], value=to_field["value"], inline=False)
                        await message.channel.send(embed=help_embed)
                        help_embed.description = None
                    help_embed.title = line.split("#")[1]
                    description_queued = True
                    field_queued = False
                    continue

                if line == "":
                    if to_field != "":
                        help_embed.add_field(name=to_field["name"], value=to_field["value"], inline=False)
                    field_queued = True
                    continue

                if line.startswith(">"):
                    line = line.replace(">", command_prefix + sub_command, 1)
                elif line.startswith("?"):
                    for override in eval(sub_command + "._command_override()").keys():
                        if line.split("?")[1].split(" ")[0] == override:
                            line = line.replace("?", override)
                            break

                if field_queued:
                    to_field["name"] = line
                    to_field["value"] = ""
                    field_queued = False
                    continue
                else:
                    to_field["value"] = to_field["value"] + line + "\n"
            return

        # Process non-help commands
        coroutine_to_execute = eval(command + "." + sub_command + "(command_args, message)")
        result = await coroutine_to_execute
        if result != "":
            await message.channel.send(result)
        return


    # By default catBot does not interact with reactions at all, and only passes off controls to command extensions.
    async def on_raw_reaction_add(self, reaction):
        checking_emoji = str(reaction.emoji)
        for extension in bot_modules:
            override_dict = eval(extension + "._reaction_override()")
            for override in override_dict.keys():
                if checking_emoji == override:
                    # Reactions passed should be explicit in wheather they are being added (True), or removed (False)
                    # Since their override must be the same function for both.
                    coroutine_to_execute = eval(extension + "." + override_dict[checking_emoji] + "(True, reaction, self)")
                    result = await coroutine_to_execute
                    if result != "":
                        await message.channel.send(result)
                    return
        return


    async def on_raw_reaction_remove(self, reaction):
        checking_emoji = str(reaction.emoji)
        for extension in bot_modules:
            override_dict = eval(extension + "._reaction_override()")
            for override in override_dict.keys():
                if str(checking_emoji) == override:
                    # Reactions passed should be explicit in wheather they are being added (True), or removed (False)
                    # Since their override must be the same function for both.
                    coroutine_to_execute = eval(extension + "." + override_dict[checking_emoji] + "(False, reaction, self)")
                    result = await coroutine_to_execute
                    if result != "":
                        await message.channel.send(result)
                    return
        return



# Run the bot.
intents = discord.Intents.default()
intents.message_content = True

token = ""
with open(token_path, "r") as token_file:
    token = token_file.read()

client = CatClient(intents=intents)
client.run(token)