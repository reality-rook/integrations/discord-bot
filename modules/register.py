import json
import mysql.connector
import string
import random

# Config management
conf_path = None
data_path = None
database = mysql.connector
config = {
            "database_host": "",
            "database_user": "",
            "database_password": "",
            "database_database": "",
            "register_url": ""
        }

def _start_up(name, given_conf_path, given_data_path):
    global conf_path
    global data_path
    global config
    global database
    conf_path = given_conf_path
    data_path = given_data_path
    try:
        with open(conf_path + "/register.conf", "r") as conf_file:
            config = json.loads(conf_file.read())
    except FileNotFoundError:
        with open(conf_path + "/register.conf", "w") as conf_file:
            conf_file.write(json.dumps(config))
        raise Exception("Valid config for the Register module not found.")

    database = database.connect(
            host = config["database_host"],
            user = config["database_user"],
            password = config["database_password"],
            database = config["database_database"]
        )



def _command_override():
    return {}


def _reaction_override():
    return {}



async def _default(args, message):
    register_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=12))
    database_cursor = database.cursor()
    sql = "INSERT INTO `auth_code` (`id`, `generated_at`, `code`, `discord_id`) VALUES (NULL, CURRENT_TIMESTAMP, %s, %s);"
    values = (register_code, str(message.author.id))
    database_cursor.execute(sql, values)
    database.commit()

    await message.author.send(register_code)
    print(database_cursor)
    return "I have sent you a message with your sign in token! You can use it at " + config["register_url"] + " to get started."



def _change_config():
    with open(conf_path + "/register.conf", "w") as conf_file:
        conf_file.write(json.dumps(config))