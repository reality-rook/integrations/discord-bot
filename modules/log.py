import json
import mysql.connector
import string
import random

# Config management
conf_path = None
data_path = None
database = mysql.connector
config = {
            "database_host": "",
            "database_user": "",
            "database_password": "",
            "database_database": "",
            "register_url": ""
        }

def _start_up(name, given_conf_path, given_data_path):
    global conf_path
    global data_path
    global config
    global database
    conf_path = given_conf_path
    data_path = given_data_path
    try:
        with open(conf_path + "/register.conf", "r") as conf_file:
            config = json.loads(conf_file.read())
    except FileNotFoundError:
        with open(conf_path + "/register.conf", "w") as conf_file:
            conf_file.write(json.dumps(config))
        raise Exception("Valid config for the Register module not found.")

    database = database.connect(
            host = config["database_host"],
            user = config["database_user"],
            password = config["database_password"],
            database = config["database_database"]
        )



def _command_override():
    return {}


def _reaction_override():
    return {}


async def _default(args, message):
    return ""

async def add(args, message):
    role_id = message.role_mentions[0].id
    database_cursor = database.cursor()
    sql = "SELECT id FROM game WHERE discord_role=" + str(role_id) + ";"
    database_cursor.execute(sql)
    matching_games = database_cursor.fetchall()
    if len(matching_games) < 1:
        return "No games use that role."

    for game in matching_games:
        new_entry_sql = 'INSERT INTO `game_order` (`id`, `game_id`, `time`) VALUES (NULL, ' + str(game[0]) + ', CURRENT_TIMESTAMP);'
        database_cursor.execute(new_entry_sql)
    database.commit()

    return "Logs updated!"



def _change_config():
    with open(conf_path + "/register.conf", "w") as conf_file:
        conf_file.write(json.dumps(config))